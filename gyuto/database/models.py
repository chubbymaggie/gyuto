from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Boolean
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Host(Base):
    __tablename__ = "hosts"
    id = Column(Integer,primary_key=True)
    # change to int later?
    ip_address = Column(String(16))


class Hostname(Base):
    __tablename__ = "hostnames"
    id = Column(Integer, primary_key=True)
    host_id = Column(Integer, ForeignKey("hosts.id"))


class Vulnerability(Base):
    __tablename__ = "vulnerabilities"
    id = Column(Integer, primary_key=True)
    detected_by = Column(String(32))
    plugin_id = Column(String(32))
    
